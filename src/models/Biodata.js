import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class Biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const { User } = models

      Biodata.belongsTo(User, { foreignKey: 'userId' })
    }
  };
  Biodata.init({
    id:{
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    }, 
    name: DataTypes.STRING,
    gender: {
      type: DataTypes.ENUM,
      values: ['m', 'f'],
    }, 
    userId: DataTypes.UUID
  }, {
    sequelize,
    modelName: 'Biodata',
    timestamps: true
  });
  return Biodata;
};

