module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Biodata', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      gender: {
        type: Sequelize.ENUM,
        values: ['m', 'f']
      },
      userId: {
        type: Sequelize.UUID,
        allowNull: true,
        onDelete: 'cascade',
        references: {
          model: 'User',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Biodata');
  }
};